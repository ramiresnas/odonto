# O que é? 
Este projeto tem como finalidade atender um consultório odontológico do IFCE campus Fortaleza. 
Este software deve fazer o controle de agendamentos e registros dos pacientes.


## Como rodar? 
### É de extrema importância que todos os passos sejam seguidos.
1. clone este repositorio
2. import para o eclipse como um projeto maven
3. crie as variáveis de ambiente no eclipe
4. selecione o projecto e vá em run > run configurations > enviroment
5. crie as seguintes variáveis
6. DATABASE_NAME : com o nome do banco **postegres** criado
7. DATABASE_PASSWORD : senha do banco
8. DATABASE_PORT : por default é 5432, caso vc tenha mudado na instalação é só alterar 
9. DATABASE_URL : localhost ou caso seja remoto a url do banco, caso remoto lembre de liberar a porta no servidor
10. DATABASE_USER : usuário do banco		
11. execute a classe [MainApp](src/main/java/MainApp.java) (na classe [MainApp](src/main/java/MainApp.java) app você pode definir a porta do servidor. O padrão é 4567)
12. acesse http://localhost:4567/(path) ou na porta que você tiver setado. 
13. na raiz do projeto crie uma pasta chamada `properties` e dentro crie um arquivo com nome `system.properties`
14. esse arquivo deve conter o seguinte valor `admin_key = [sua_senha_secreta]` essa é a senha utilizada para realizar a chamada a api para realizar o cadastro de usuário
15. caso você queira usar a mesma da documentação : `YNSp0BPiSBMDBpNagsoIuzerItFU7G1WUJCKKRteT6hUmwwIeaRPOw7YfCsdJkzeu1p6JZadW7z9tkINAadPLbnreXKn8AIs1zo`
16. utilize a [documentação](https://documenter.getpostman.com/view/1700540/ifce/77h84JQ) para realizar teste