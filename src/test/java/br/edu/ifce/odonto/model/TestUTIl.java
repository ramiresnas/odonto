package br.edu.ifce.odonto.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Horario;
import br.edu.ifce.odonto.models.Usuario;
import br.edu.ifce.odonto.util.GsonUtil;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class TestUTIl {
	private static final String URL = "http://localhost:4567/api/";
	private static final String key = "YNSp0BPiSBMDBpNagsoIuzerItFU7G1WUJCKKRteT6hUmwwIeaRPOw7YfCsdJkzeu1p6JZadW7z9tkINAadPLbnreXKn8AIs1zo";
	private static Gson gson = GsonUtil.getInstance();

	public static Dentista getDentista() {
		Dentista dentista = new Dentista("Helena Souza", "1298-45", getFones());
		dentista.setHorariosDeAtendimento(getHorariosDeAtendiemnto());
		return dentista;
	}

	private static List<String> getFones() {
		List<String> fones = new ArrayList<>();
		fones.add("(85) 3284-2959");
		fones.add("(85) 98585-2014");
		return fones;
	}

	private static List<Horario> getHorariosDeAtendiemnto() {
		List<Horario> horarios = new ArrayList<>();
		Horario horario1 = horario(13, 30, DayOfWeek.TUESDAY);
		Horario horario2 = horario(12, 30, DayOfWeek.TUESDAY);
		Horario horario3 = horario(9, 30, DayOfWeek.MONDAY);
		Horario horario4 = horario(10, 30, DayOfWeek.MONDAY);
		Horario horario5 = horario(8, 30, DayOfWeek.MONDAY);
		Horario horario6 = horario(15, 30, DayOfWeek.WEDNESDAY);
		horarios.add(horario1);
		horarios.add(horario2);
		horarios.add(horario3);
		horarios.add(horario4);
		horarios.add(horario5);
		horarios.add(horario6);
		return horarios;
	}

	private static Horario horario(int hora, int minuto, DayOfWeek dayOfWeek) {
		Horario horario1 = new Horario();
		horario1.setHora(LocalTime.of(hora, minuto));
		horario1.setDayOfWeek(dayOfWeek);
		return horario1;
	}

	public static Request get(String url) {
		Request request = new Request.Builder().url(TestUTIl.URL + url).get()
				.addHeader("Authorization", "Basic NTQzMjE6MTIwMjEw").build();
		return request;
	}

	public static Request post(String url, Object data) {
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, gson.toJson(data));
		Request request = new Request.Builder().url(TestUTIl.URL + url).post(body)
				.addHeader("Content-Type", "application/json").addHeader("Authorization", "Basic NTQzMjE6MTIwMjEw")
				.addHeader("private_key", key).build();
		return request;
	}

	public static Usuario getUsuario() {
		Usuario usuario = new Usuario("54321", "120210");
		usuario.setNome("Edson Silva");
		return usuario;
	}
}
