package br.edu.ifce.odonto.model;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.util.GsonUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DentistaTest {

	Gson gson;
	OkHttpClient client;
	Dentista dentista;
	
	@Before
	public void init() {
		gson = GsonUtil.getInstanceForTests();
		client = new OkHttpClient();
		dentista = TestUTIl.getDentista();
	}
	
	@Test
	public void a_deveCadastarUmDentista() throws IOException {
		Request request = TestUTIl.post("dentista/add", TestUTIl.getDentista());
		Response response = client.newCall(request).execute();
		String stringResult = response.body().string();
		System.out.println(stringResult);
		Mensagem mensagem = gson.fromJson(stringResult, Mensagem.class);
		Assert.assertTrue(mensagem.getMsg(),mensagem.isSuccess());
	}
	
	@Test
	public void b_deveObterTodosOsDentistas() throws IOException {
		Request request = TestUTIl.get("dentista/");
		Response response = client.newCall(request).execute();
		String stringResutl = response.body().string();
		System.out.println(stringResutl);
		Type listType = new TypeToken<ArrayList<Dentista>>(){}.getType();
		List<Dentista> dentistas = gson.fromJson(stringResutl, listType);
		Assert.assertEquals(1, dentistas.size());
		Assert.assertEquals(dentistas.get(0).getCro(), TestUTIl.getDentista().getCro());
	}
	
	@Test
	public void c_deveObterDentistaPeloCRO() throws IOException {
		Request request = TestUTIl.get("dentista/cro/"+dentista.getCro());
		Response response = client.newCall(request).execute();
		Dentista dentistaFound = gson.fromJson(response.body().string(), Dentista.class);
		System.out.println("DentistaTest.c_deveObterDentistaPeloCRO()");
		Assert.assertEquals(dentista.getCro(), dentistaFound.getCro());
	}
}
