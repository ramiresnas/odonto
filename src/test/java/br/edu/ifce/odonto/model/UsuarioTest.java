package br.edu.ifce.odonto.model;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.google.gson.Gson;

import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Usuario;
import br.edu.ifce.odonto.util.GsonUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsuarioTest {

	Gson gson;
	OkHttpClient client;
	Usuario usuario;
	
	@Before
	public void init() {
		gson = GsonUtil.getInstanceForTests();
		client = new OkHttpClient();
		usuario = TestUTIl.getUsuario();
	}
	
	@Test
	public void a_deveCadastrarUsuario() throws IOException { 
		Request request = TestUTIl.post("usuario/add", usuario);
		Response response = client.newCall(request).execute();
		String StringResutl = response.body().string();
		System.out.println(StringResutl);
		Mensagem mensagem = gson.fromJson(StringResutl, Mensagem.class);
		Assert.assertTrue(mensagem.getMsg(), mensagem.isSuccess());
	}
	
	@Test 
	public void b_naoDeveCadastarOMesmoUsuario() throws IOException {
		Request request = TestUTIl.post("usuario/add", usuario);
		Response response = client.newCall(request).execute();
		String stringResult = response.body().string();
		System.out.println(stringResult);
		Mensagem mensagem = gson.fromJson(stringResult, Mensagem.class);
		Assert.assertFalse(mensagem.getMsg(), mensagem.isSuccess());
	}
	
	@Test
	public void c_deveFazerLogin() throws IOException {
		Request request = TestUTIl.post("usuario/logar", usuario);
		Response response = client.newCall(request).execute();
		String stringResult = response.body().string();
		System.out.println(stringResult);
		Mensagem mensagem = gson.fromJson(stringResult, Mensagem.class);
		Assert.assertTrue(mensagem.getMsg(), mensagem.isSuccess());
	}
	
}
