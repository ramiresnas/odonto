package br.edu.ifce.odonto.adapter;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import br.edu.ifce.odonto.enums.PacienteEnum;
import br.edu.ifce.odonto.models.Paciente;

public class PacienteAdapter implements JsonDeserializer<Paciente> {

	@Override
	public Paciente deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		
		JsonObject jsonObject = json.getAsJsonObject();
		String tipoStr = jsonObject.get("tipo").getAsString();
		Class<?> clazz = PacienteEnum.valueOf(tipoStr).getValue();
		return context.deserialize(json, clazz);

	}
}