package br.edu.ifce.odonto.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final ThreadLocal<Session> threadLocal = new ThreadLocal<Session>();
	private static SessionFactory sessionFactory = null;

	public static Session openSession() {
		final ProcessBuilder processBuilder = new ProcessBuilder();
		final String host = processBuilder.environment().get("DATABASE_HOST");
		final int port = Integer.parseInt(processBuilder.environment().get("DATABASE_PORT"));
		final String base = processBuilder.environment().get("DATABASE_NAME");
		final String login = processBuilder.environment().get("DATABASE_USER");
		final String passwd = processBuilder.environment().get("DATABASE_PASSWORD");
		final Configuration config = new Configuration().configure(getPath());
		config.setProperty("hibernate.connection.url", "jdbc:postgresql://" + host + ":" + port +"/"+ base);
		config.setProperty("hibernate.connection.username", login);
		config.setProperty("hibernate.connection.password", passwd);
		sessionFactory = config.buildSessionFactory();
		threadLocal.set(sessionFactory.openSession());
		Session session = threadLocal.get();
		return session;
	}

	public static void closeCurrentSession() {
		threadLocal.get().close();
		threadLocal.set(null);
	}

	public static Session getCurrentSession() {
		return threadLocal.get();
	}
	
	public static final String getPath() {	
		return "hibernate.cfg.xml";	    
	}
}
