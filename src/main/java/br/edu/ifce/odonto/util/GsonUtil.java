package br.edu.ifce.odonto.util;

import java.time.LocalDateTime;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.edu.ifce.odonto.adapter.HibernateProxyTypeAdapter;
import br.edu.ifce.odonto.adapter.LocalTimeAdapter;
import br.edu.ifce.odonto.adapter.PacienteAdapter;
import br.edu.ifce.odonto.models.Paciente;

public class GsonUtil {
	
	private static Gson gson;
	
	public static Gson getInstance(){
		if( gson == null) {
			ExcludeFields excludeFields = new ExcludeFields();
			gson = new GsonBuilder().setPrettyPrinting()
			        .registerTypeAdapter(LocalDateTime.class, new LocalTimeAdapter())
			        .registerTypeAdapter(Paciente.class, new PacienteAdapter())
			        .addSerializationExclusionStrategy(excludeFields)
			        .registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY).create();
		}
		return gson;
	}
	
	public static Gson getInstanceForTests(){
		if( gson == null) {
			gson = new GsonBuilder().setPrettyPrinting()
			        .registerTypeAdapter(LocalDateTime.class, new LocalTimeAdapter())
			        .registerTypeAdapter(Paciente.class, new PacienteAdapter())
			        .registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY).create();
		}
		return gson;
	}
}

class ExcludeFields implements ExclusionStrategy{

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getName().equals("password");
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
	
}