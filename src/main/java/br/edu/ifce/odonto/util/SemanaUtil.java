package br.edu.ifce.odonto.util;

import java.time.DayOfWeek;
import java.util.HashMap;
import java.util.Map;

public class SemanaUtil {
	
	
	public static Map<String, DayOfWeek> getDays(){
		Map<String, DayOfWeek> dias = new HashMap<>();
		dias.put("SEGUNDA-FEIRA", DayOfWeek.MONDAY);
		dias.put("TERÇA-FEIRA", DayOfWeek.TUESDAY);
		dias.put("QUARTA-FEIRA", DayOfWeek.WEDNESDAY);
		dias.put("QUINTA-FEIRA", DayOfWeek.THURSDAY);
		dias.put("SEXTA-FEIRA", DayOfWeek.FRIDAY);
		dias.put("SÁBADO", DayOfWeek.SATURDAY);
		dias.put("DOMINGO", DayOfWeek.SUNDAY);
		return dias;
	}
}
