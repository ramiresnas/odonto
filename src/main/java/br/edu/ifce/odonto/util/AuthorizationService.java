package br.edu.ifce.odonto.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Collections;
import java.util.List;

import br.edu.ifce.odonto.DAO.UsuarioDao;
import br.edu.ifce.odonto.models.Usuario;
import spark.Request;
import spark.Response;
import spark.Spark;

public class AuthorizationService {
		
	/**
	 * @param reqquest
	 *            {@linkplain Request}
	 * @param response
	 *            {@linkplain Response}
	 */
	public static void authorize(final Request reqquest, final Response response) {
		final String token = reqquest.headers("Authorization");
		if (isNullOrEmpety(token) || invalidHeader(token) || !isValidCredentias(token.split(" ")[1]))
			Spark.halt(401);
		response.status(200);
	}

	private static boolean isValidCredentias(final String hash) {
		final Decoder decoder = Base64.getDecoder();
		final byte[] bytesDecoded = decoder.decode(hash.getBytes());
		final String stringDecode = new String(bytesDecoded, StandardCharsets.UTF_8);
		final String[] credentials = stringDecode.split(":");
		try {
			final String matricula = credentials[0];
			final String password = credentials[1];
			final String passwordMD5 = getMD5(password);
			final Usuario usuario = new Usuario(matricula, passwordMD5);
			UsuarioDao usuarioDao = new UsuarioDao();
			final Usuario usuarioFound = usuarioDao.get(usuario);
			if (usuarioFound != null) {
				return true;
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}

	static boolean isNullOrEmpety(final String str) {
		return str == null || str.isEmpty();
	}

	static boolean invalidHeader(final String basicHash) {
		return basicHash.split(" ").length < 1 || !basicHash.split(" ")[0].equals("Basic");
	}
	
	public static final String getMD5(String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(password.getBytes(),0,password.length());
			String result = new BigInteger(1,digest.digest()).toString(16);
			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getRandomPassword(final int qtdeMaximaCaracteres) {
        final String[] character = { "a", "1", "b", "2", "4", "5", "6", "7", "8",
                "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
                "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
                "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
                "V", "W", "X", "Y", "Z" };
       
        final StringBuilder randomPassword = new StringBuilder();
        List<String> list = Arrays.asList(character);
        Collections.shuffle(list);
        for (int i = 0; i < qtdeMaximaCaracteres; i++) {
            int posicao = (int) (Math.random() * character.length);
            randomPassword.append(list.get(posicao));
        }
        return randomPassword.toString();
    }
}
