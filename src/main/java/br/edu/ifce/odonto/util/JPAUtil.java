package br.edu.ifce.odonto.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

public class JPAUtil {
	private static EntityManagerFactory factory;
	private static ThreadLocal<EntityManager> threadEntityManager = new ThreadLocal<EntityManager>();

	/**
	 * @return a singleton instance of the {@link EntityManager}
	 */
	public static final EntityManager getEntityManager() {
		if (factory == null) {
			factory = HibernateUtil.openSession().getEntityManagerFactory();
		}
		EntityManager entityManager = threadEntityManager.get();
		if (entityManager == null || !entityManager.isOpen()) {
			entityManager = factory.createEntityManager();
			JPAUtil.threadEntityManager.set(entityManager);
		}
		return entityManager;
	}

	private static void closeEntityManager() {
		EntityManager em = threadEntityManager.get();
		if (em != null) {
			EntityTransaction transaction = em.getTransaction();
			if (transaction.isActive()) {
				transaction.commit();
			}
			em.close();
			threadEntityManager.set(null);
		}
	}
	
	public static void closeEntityManagerFactory() {
	       closeEntityManager();
	       factory.close();
	}
}
