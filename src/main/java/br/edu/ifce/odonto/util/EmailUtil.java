package br.edu.ifce.odonto.util;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.edu.ifce.odonto.models.Paciente;

public class EmailUtil {
	
	public static void send(String subject,String msg,Paciente paciente) {
		String emailFrom;
		String password;
		try {
			ProcessBuilder processBuilder = new ProcessBuilder();
			emailFrom = processBuilder.environment().get("email");
			password = processBuilder.environment().get("password_email");
		}catch (Exception e) {
			e.printStackTrace();
			return;
		}
		HtmlEmail email = new HtmlEmail();
		email.setHostName("smtp.googlemail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator(emailFrom, password));
		email.setSSLOnConnect(true);
		StringBuilder builder = new StringBuilder();
		builder.append("<html><body>").append(msg).append("</body></html>");
		try {
			email.setContent(email.getMimeMessage(), "text/html; charset=utf-8");
			email.setFrom(emailFrom);
			email.setSubject(subject);
			email.setHtmlMsg(builder.toString());
			email.setTextMsg(msg);
			email.addTo(paciente.getEmail());
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}
}
