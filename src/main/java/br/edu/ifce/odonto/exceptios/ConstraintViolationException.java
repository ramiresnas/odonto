package br.edu.ifce.odonto.exceptios;

public class ConstraintViolationException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msg;
	
	public ConstraintViolationException(String msg) {
		this.msg = msg;	
	}
	
	@Override
	public String getMessage() {
		return msg;
	}

}
