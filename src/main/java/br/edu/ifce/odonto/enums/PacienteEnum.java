package br.edu.ifce.odonto.enums;

import br.edu.ifce.odonto.models.Dependente;
import br.edu.ifce.odonto.models.Discente;
import br.edu.ifce.odonto.models.Servidor;

public enum PacienteEnum {
	DISCENTE(Discente.class), SERVIDOR(Servidor.class), DEPENDENTE(Dependente.class);
	// TERCEIRIZADO(Terceirizado.class);

	private Class<?> clazz;

	PacienteEnum(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	public Class<?> getValue() {
		return clazz;
	}
	
}
