package br.edu.ifce.odonto.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Recuperacao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	private String email;
	private String codigo;
	private LocalDateTime dateTime;
	private String password;
	
	public Recuperacao() {
	}
	
	public Recuperacao(Paciente paciente, String codigo) {
		dateTime = LocalDateTime.now();
		email = paciente.getEmail();
		this.codigo = codigo;
	}

	public String getEmail() {
		return email;
	}

	public String getCodigo() {
		return codigo;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public String getPassword() {
		return password;
	}

	
}
