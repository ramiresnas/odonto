package br.edu.ifce.odonto.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import br.edu.ifce.odonto.DAO.AgendamentoDAO;
import br.edu.ifce.odonto.enums.PacienteEnum;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Paciente  {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	private String nome;
	
	@Column
	private int idade;
	@Column(nullable = false, unique = true)
	private String matricula;
	private String email;
	private String password;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "dentista_id")
	private Dentista dentista;

	@Column(name = "tipo")
	@Enumerated(EnumType.STRING)
	protected PacienteEnum tipo;

	@OneToMany(mappedBy = "paciente", targetEntity = Agendamento.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private transient List<Agendamento> agendamentos;

	private transient boolean cache = false;
	
	public Paciente() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public Dentista getDentista() {
		return dentista;
	}

	public void setDentista(Dentista dentista) {
		this.dentista = dentista;
	}

	public PacienteEnum getTipo() {
		return tipo;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paciente other = (Paciente) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Agendamento> getAgendamentos() {
		if(!cache) {
			AgendamentoDAO agendamentoDAO = new AgendamentoDAO();
			agendamentos = agendamentoDAO.getAgendamentosByPaciente(this.id);
			cache = true;
		}
		return agendamentos;
	}
	
}
