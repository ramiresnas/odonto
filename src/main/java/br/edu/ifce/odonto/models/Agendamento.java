
package br.edu.ifce.odonto.models;

import java.time.DayOfWeek;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Agendamento implements Comparable<Agendamento> {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=javax.persistence.CascadeType.ALL)
	@JoinColumn(name="dentista_id")
	Dentista dentista;
	
	@ManyToOne
	private Paciente paciente;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=javax.persistence.CascadeType.ALL)
	@JoinColumn(name="horario_id")
	private Horario horario;
	
	@Column
	private String observacao;
	
	private boolean compareceu;
	
	@Column(columnDefinition="boolean default false")
	private boolean retorno;
	
	@Column(columnDefinition="boolean default true")
	private boolean ativo;
	
	@Column(columnDefinition="boolean default false")
	private boolean cancelado;
	
	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	
	public Dentista getDentista() {
		return dentista;
	}
	
	public void setDentista(Dentista dentista) {
		this.dentista = dentista;
	}

	public Horario getHorario() {
		return horario;
	}
	
	public void setHorario(Horario horario) {
		this.horario = horario;
	}
	
	public DayOfWeek getDayOfWeek() {
		return horario.getDayOfWeek();
	}
	
	public boolean compareceu() {
		return compareceu;
	}

	@Override
	public int compareTo(Agendamento other) {
		return this.horario.compareTo(other.horario);
	}

	public boolean isAtivo() {
		return ativo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agendamento other = (Agendamento) obj;
		if (dentista == null) {
			if (other.dentista != null)
				return false;
		} else if (!dentista.equals(other.dentista))
			return false;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dentista == null) ? 0 : dentista.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		return result;
	}
}
