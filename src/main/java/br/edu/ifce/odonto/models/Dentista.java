
package br.edu.ifce.odonto.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.edu.ifce.odonto.util.ComparadorHorario;

@Entity
public class Dentista {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	@Column(unique=true,nullable=false)
	private String nome;
	@Column(unique=true,nullable=false)
	private String cro;
	
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	@Column(nullable=false,name="fones")
	@Cascade(CascadeType.ALL)
	private List<String> fones;

	
	@ManyToMany(fetch=FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@Column(nullable=false)
	private List<Horario> horariosDeAtendimento = new ArrayList<Horario>();
	
	
	public Dentista() {
	}

	public Dentista(String nome, String cro, List<String> fones) {	
		this.nome = nome;
		this.cro = cro;
		this.fones = fones;
	}
	
	public void addHorariosDeAtendimento(Collection<Horario> collection) {
		horariosDeAtendimento.addAll(collection);
	}

	public List<Horario> getHorariosDeAtendimento() {
		horariosDeAtendimento.sort(new ComparadorHorario());
		return horariosDeAtendimento;
	}
	
	public Integer getId() {
		return id;
	}	

	public String getCro() {
		return cro;
	}

	public void setHorariosDeAtendimento(List<Horario> horariosDeAtendimento) {
		this.horariosDeAtendimento = horariosDeAtendimento;
	}	

	public boolean atende(Horario horario) {
		for (Horario h : horariosDeAtendimento) {
			if(h.getDayOfWeek().equals(horario.getDayOfWeek()) && h.getHora().equals(horario.getHora()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cro == null) ? 0 : cro.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dentista other = (Dentista) obj;
		if (cro == null) {
			if (other.cro != null)
				return false;
		} else if (!cro.equals(other.cro))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	

}
