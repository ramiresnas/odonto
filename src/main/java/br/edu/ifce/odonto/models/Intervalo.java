package br.edu.ifce.odonto.models;

import java.time.LocalDate;

import org.eclipse.jetty.util.ArrayUtil;

import spark.Request;

public class Intervalo {
	private LocalDate[] datas;
	
	
	public Intervalo(Request req) {
		int v[] = new int[6];
		final String strDataInicio = req.params(":dataInicio");
		final String strDataFim = req.params(":dataFim");
		final String[] inicioSplit = strDataInicio.split("-");
		final String[] fimSplit = strDataFim.split("-");
		String[] splists = ArrayUtil.add(inicioSplit, fimSplit);
		for (int i = 0; i < splists.length; i++) {
			v[i] = Integer.parseInt(splists[i]);
		}
		final LocalDate dataInicio = LocalDate.of(v[0], v[1], v[2]);
		final LocalDate dataFim = LocalDate.of(v[3], v[4], v[5]);
		datas = new LocalDate[] {dataInicio,dataFim};
	}
	public LocalDate getInicio() {
		return datas[0];
	}
	
	public LocalDate getFim() {
		return datas[1];
	}	
}
