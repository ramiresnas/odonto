package br.edu.ifce.odonto.models;

import javax.persistence.Entity;

import br.edu.ifce.odonto.enums.PacienteEnum;

@Entity
public class Discente extends Paciente {

	public Discente() {
		super();
		this.tipo = PacienteEnum.DISCENTE;
	}

}
