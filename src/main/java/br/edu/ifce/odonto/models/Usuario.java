package br.edu.ifce.odonto.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column
	private String nome;
	@Column(unique=true)
	private String matricula;
	private String password;

	public Usuario() {
	
	}
	
	public Usuario(String matricula, String password) {
		this.matricula = matricula;
		this.password = password;
	}
	
	public Usuario(String nome, String matricula, String password) {
		this(matricula, password);
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Usuario [email=" + matricula + ", password=" + password + "]";
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
