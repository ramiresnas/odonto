package br.edu.ifce.odonto.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.edu.ifce.odonto.enums.PacienteEnum;

@Entity
public class Dependente extends Paciente {

	@ManyToOne(optional=false,cascade=CascadeType.REMOVE)
	@JoinColumn(name = "titular_id")
	private Servidor titular;

	public Dependente() {
		super();
		this.tipo = PacienteEnum.DEPENDENTE;
	}

}
