
package br.edu.ifce.odonto.models;

/**
 * Uma {@link Mensagem} representa uma resposta à uma ação executada. 
 * Uma {@linkplain Mensagem} contém uma {@link String} que é a mensagem e um
 * {@link Boolean} que é o status da ação, e pode opcionalmente ter dados,
 * que pode ser qualquer {@link Object}
 * @author Ramires Moreira
 */
public class Mensagem {

	private String msg;
	private boolean success;

	@SuppressWarnings("unused")
	private Object data;

	public Mensagem(String msg,boolean success){
		this.msg = msg;
		this.success = success;
	}
	
	public Mensagem(String mensage,boolean success,Object data) {
		msg = mensage;
		this.success = success;
		this.data = data;
	}
	
	public String getMsg() {
		return msg;
	}

	public boolean isSuccess() {
		return success;
	}
	
}
