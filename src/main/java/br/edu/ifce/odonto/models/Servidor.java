package br.edu.ifce.odonto.models;

import javax.persistence.Entity;

import br.edu.ifce.odonto.enums.PacienteEnum;

@Entity
public class Servidor extends Paciente {

	public Servidor() {
		this.tipo = PacienteEnum.SERVIDOR;
	}

}
