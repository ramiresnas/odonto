package br.edu.ifce.odonto.controllers;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.persistence.NoResultException;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.PacienteDAO;
import br.edu.ifce.odonto.DAO.RecuperacaoDAO;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.models.Recuperacao;
import br.edu.ifce.odonto.util.AuthorizationService;
import br.edu.ifce.odonto.util.EmailUtil;
import br.edu.ifce.odonto.util.GsonUtil;
import spark.Request;
import spark.Response;

public class AcessoController {
	RecuperacaoDAO recuperacaoDAO = new RecuperacaoDAO();
	PacienteDAO pacienteDAO = new PacienteDAO();
	
	public Mensagem sendCode(Request req, Response resp) {
		try {
			Gson gson = GsonUtil.getInstance();
			Paciente paciente = gson.fromJson(req.body(), Paciente.class);
	
			String randomCod = AuthorizationService.getRandomPassword(8);
			String msg = "Seu código de recuperação de senha é  <a href=\"ifceodonto://recuperar/"+randomCod+"\">"
					 + randomCod + "</a>";
			Recuperacao recuperacao = new Recuperacao(paciente, randomCod);
			 
			recuperacaoDAO.save(recuperacao);
			EmailUtil.send("Recuperação de senha	", msg, paciente);
			return new Mensagem("Seu código de recuperação foi enviado para seu email", true);
		}catch (NoResultException e) {
			e.printStackTrace();
			return new Mensagem("não foi possível encontrar nenhum paciente com esse email", false);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return new Mensagem(e.getMessage(), false);
		}
	}

	public Mensagem recuperarSenha(Request req, Response resp) {
		try {			
			Gson gson = GsonUtil.getInstance();
			Recuperacao recuperacao = gson.fromJson(req.body(), Recuperacao.class);
			Optional<Recuperacao> optionalRecuperacao = recuperacaoDAO.findByCodigo(recuperacao);
			if (optionalRecuperacao.isPresent()) {
				Recuperacao recuperacaoFound = optionalRecuperacao.get();
				if(recuperacaoFound.getDateTime().plusDays(1).compareTo(LocalDateTime.now()) < 0) {
					return new Mensagem("Erro: esse código já perdeu a validade!", false);
				}
				Paciente paciente = pacienteDAO.findByEmail(recuperacao.getEmail());
				String password = AuthorizationService.getMD5(recuperacao.getPassword());
				paciente.setPassword(password);
				pacienteDAO.update(paciente);
				recuperacaoDAO.remove(recuperacaoFound);
				return new Mensagem("Senha recuperada com sucesso!", true);
			} else {
				return new Mensagem("Erro ao tentar atualizar a senha, tente novamente!", false);
			}
		}catch (NoResultException e) {
			return new Mensagem("Erro: esse código já foi utilizado ou não existe!", false);
		}
	}
}
