package br.edu.ifce.odonto.controllers;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.UsuarioDao;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Usuario;
import br.edu.ifce.odonto.util.AuthorizationService;
import br.edu.ifce.odonto.util.GsonUtil;
import spark.Request;
import spark.Response;

public class UsuarioController {
	
	UsuarioDao usuarioDao = new UsuarioDao();
	
	public Mensagem add(final Request req, final Response resp) {
		String key = getProp(); 
		String privateKey = req.headers("private_key");
		if(privateKey == null || privateKey.isEmpty() || !privateKey.equals(key)) {
			resp.status(401);
			return new Mensagem("Acesso negado", false);
		}
			
		try {
			final Gson gson = GsonUtil.getInstance();
			final Usuario user = gson.fromJson(req.body(), Usuario.class);
			if (user == null || user.getMatricula() == null || user.getPassword() == null)
				throw new Exception("formato inválido");
			if (user.getMatricula().isEmpty() || user.getPassword().isEmpty()) {
				throw new Exception("passe um email e senha válido");
			}
			final String md5 = AuthorizationService.getMD5(user.getPassword());
			user.setPassword(md5);
			usuarioDao.save(user);
			return new Mensagem("Usuário adcionado com sucesso", true);
		}catch (Exception e) {
			e.printStackTrace();
			return new Mensagem(e.getMessage(), false);
		}
	}

	public Mensagem logar(Request req, Response resp) {
		AuthorizationService.authorize(req, resp);
		if(resp.status() == 200) {
			return new Mensagem("Usuário logado com sucesso", true);
		}
		return new Mensagem("Credenciais inválidas", false);
	}
	
	private String getProp() {
		final ProcessBuilder processBuilder = new ProcessBuilder();
		return processBuilder.environment().get("admin_key");
	}

}
