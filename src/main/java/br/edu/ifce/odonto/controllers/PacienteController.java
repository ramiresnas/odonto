package br.edu.ifce.odonto.controllers;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.DentistaDAO;
import br.edu.ifce.odonto.DAO.PacienteDAO;
import br.edu.ifce.odonto.enums.PacienteEnum;
import br.edu.ifce.odonto.models.Agendamento;
import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Dependente;
import br.edu.ifce.odonto.models.Discente;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.models.Servidor;
import br.edu.ifce.odonto.util.AuthorizationService;
import br.edu.ifce.odonto.util.EmailUtil;
import br.edu.ifce.odonto.util.GsonUtil;
import spark.Request;
import spark.Response;

public class PacienteController {
	
	
	private PacienteDAO pacienteDAO = new PacienteDAO();
	
	private DentistaDAO dentistaDAO = new DentistaDAO();
	
	Gson gson = GsonUtil.getInstance();
	Mensagem mensagem;

	public Mensagem addUser(Request req, Response resp) throws Exception {
		String randomPassword = AuthorizationService.getRandomPassword(8);
		boolean isRandomPassword = false;
		try {
			Paciente paciente = getPaciente(req);
			if (paciente == null) {
				throw new Exception("Paciente inválido");
			}
			if (paciente.getPassword() == null || paciente.getPassword().isEmpty()) {
				paciente.setPassword(AuthorizationService.getMD5(randomPassword));
				isRandomPassword = true;
			}else {
				randomPassword = paciente.getPassword();
				paciente.setPassword(AuthorizationService.getMD5(randomPassword));
			} 
				
			Integer dentistaId = paciente.getDentista().getId();

			if (dentistaId == null)
				throw new Exception("Selecione um dentista");
			if (paciente.getTipo() == null)
				throw new Exception("Selecione um tipo de paciente válido!");

			if (podeSeCadastrar(paciente)) {
				
				Dentista dentista = dentistaDAO.get(dentistaId);
				paciente.setDentista(dentista);
				pacienteDAO.save(paciente);
				final String password = randomPassword;
				new Thread(()-> {
					String msg = "sua senha é "+ password + " !";
					EmailUtil.send("Cadastro efetuado com sucesso",msg, paciente);
				}).start();
				String msg = isRandomPassword ? " Verifique seu email para consultar a senha de acesso." : ""; 
				mensagem = new Mensagem(paciente.getNome() + " você foi cadastrado com sucesso!"+msg, true);
			} else {
				mensagem = new Mensagem("Error: O paciente não é elegígel para se cadastar", false);
			}
			return mensagem;
		} catch (Exception e) {
			e.printStackTrace();
			return new Mensagem("já existe um paciente com essa matricula", false);
		}
	}

	public Paciente get(Request req, Response resp) {
		try {
			final int id = Integer.parseInt(req.params(":id"));
			final Paciente paciente = new Discente();
			paciente.setId(id);
			return pacienteDAO.get(paciente);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Collection<Paciente> getAll(Request req) {
		try {
			final int pagina = Integer.parseInt(req.params(":pagina"));
			final int quantidade = Integer.parseInt(req.params(":quantidade"));
			return pacienteDAO.getAll(pagina, quantidade);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static boolean podeSeCadastrar(Paciente paciente) {
		return !paciente.getMatricula().isEmpty();
	}

	public Mensagem addDependente(Request req, Response resp) {
		try {
			Dependente dependente = gson.fromJson(req.body(), Dependente.class);
			String password = AuthorizationService.getRandomPassword(8);
			String md5 = AuthorizationService.getMD5(password);
			dependente.setPassword(md5);
			pacienteDAO.update(dependente);
			return new Mensagem("Dependente adcionado com sucesso!", true);
		} catch (Exception e) {
			e.printStackTrace();
			return new Mensagem("Erro inesperado ", false);
		}
		
	}

	public Paciente findByMatricula(Request req, Response resp) throws Exception {
		String matricula = req.params(":matricula");
		try {
			final Paciente p = new Discente();
			p.setMatricula(matricula);
			final Paciente paciente = pacienteDAO.findByMatricula(p);
			return paciente;
		} catch (Exception e) {
			resp.status(400);
			mensagem = new Mensagem("Não encontamos nenhum paciente com a matricula " + matricula + ".", false);
			resp.body(gson.toJson(mensagem));
			return null;
		}
	}

	private Paciente getPaciente(Request req) {
		Paciente paciente = gson.fromJson(req.body(), Servidor.class);
		final PacienteEnum tipo = paciente.getTipo();
		if (tipo == null || tipo.name().isEmpty() || paciente.getMatricula() == null
				|| paciente.getMatricula().isEmpty())
			return null;

		if (tipo == PacienteEnum.SERVIDOR)
			return paciente;
		else if (tipo == PacienteEnum.DISCENTE)
			return gson.fromJson(req.body(), Discente.class);
		return null;
	}

	public List<Agendamento> getAgendamentos(Request req, Response resp) {
		try {
			Paciente paciente = findByMatricula(req, resp);
			List<Agendamento> list = paciente.getAgendamentos();
			return list.stream()
			.filter(a -> a.isAtivo())
			.collect(Collectors.toList()); 
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}

	public Mensagem login(Request req, Response resp) {
		try {
			Paciente paciente = gson.fromJson(req.body(), Discente.class);
			paciente =  pacienteDAO.findByMatriculaAndPassword(paciente);
			return new Mensagem("Login efetuado com sucesso "+paciente.getNome(),true,paciente);
		} catch (NullPointerException npe) {
			return new Mensagem("Usuário ou senha inválido", false);
		}
	}
}
