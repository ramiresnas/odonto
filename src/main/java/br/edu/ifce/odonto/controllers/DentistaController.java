package br.edu.ifce.odonto.controllers;

import java.util.Collection;
import java.util.Collections;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.DentistaDAO;
import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.util.GsonUtil;
import spark.Request;
import spark.Response;

public class DentistaController {
	
	private DentistaDAO dao = new DentistaDAO();
	
	private Gson gson = GsonUtil.getInstance();
	
	public Mensagem addDentista(Request req , Response resp) throws Exception{
		Dentista dentista = gson.fromJson(req.body(), Dentista.class);
		try {
			Collections.sort(dentista.getHorariosDeAtendimento());
		} catch (Exception e) {
			return new Mensagem("Dados inválidos", true);
		}
		return salvar(dentista);
	}

	private Mensagem salvar(Dentista dentista) {
		Mensagem msg;
		boolean saved = false;
		try {
			saved = dao.save(dentista);
			msg = new Mensagem("Dentista addcionado com sucesso!", saved);
		} catch (Exception e) {
			e.printStackTrace();
			msg = new Mensagem(e.getMessage(), saved);
		}
		return msg;
	}

	public Collection<Dentista> getAll() {
		return dao.getAll();
	}

	public Dentista get(Request req, Response resp) throws InterruptedException {
		 return dao.get(Integer.parseInt(req.params(":id")));
	}
	
	public Mensagem addHorarios(Request req){
		  Dentista dentista = gson.fromJson(req.body(), Dentista.class);
		  Dentista dentistaAtual = dao.get(dentista.getId());
		  dentistaAtual.addHorariosDeAtendimento(dentista.getHorariosDeAtendimento());
		  dao.update(dentistaAtual);
		  return new Mensagem("horarios adicionados com sucesso", true);
	}

	public Mensagem update(Request req) {	  
		try {
		  Dentista dentista = gson.fromJson(req.body(), Dentista.class);
		  dao.update(dentista);
		  return new Mensagem("dados atualizados com sucesso", true);
		}catch (Exception e) {
			e.printStackTrace();
			return new Mensagem("Erro ao tentar atualizar os dados, verifique se as informações estão corretas", false);
		}
	}

	public Dentista findByCRO(Request req) {
		try {
			return dao.findByCRO(req.params(":cro"));
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Mensagem delete(Request req,Response resp) {
		try {
			Dentista dentista = gson.fromJson(req.body(),Dentista.class);
			dao.delete(dentista);
			return new Mensagem("dentista deletado com sucesso", true);
		}catch (Exception e) {
			e.printStackTrace();
			return new Mensagem("erro ao remover o dentista", false);
		}
	}

}
