package br.edu.ifce.odonto.controllers;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.AgendamentoDAO;
import br.edu.ifce.odonto.DAO.DentistaDAO;
import br.edu.ifce.odonto.DAO.PacienteDAO;
import br.edu.ifce.odonto.models.Agendamento;
import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Horario;
import br.edu.ifce.odonto.models.Intervalo;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.util.GsonUtil;
import spark.Request;
import spark.Response;

/**
 * @author Ramires Moreira
 */
public class AgendamentoController {

	private AgendamentoDAO agendamentoDAO = new AgendamentoDAO();
	private DentistaDAO dentistaDAO = new DentistaDAO();
	private PacienteDAO pacienteDAO = new PacienteDAO();
	private Gson gson = GsonUtil.getInstance();
	private static final int MAX_AGENDAMENTOS_PERMITIDOS = 14;

	/**
	 * Efetua um agendamento.
	 * @param req -  {@link spark.Request}
	 * @param resp - {@link spark.Response}
	 * @return {@link Mensagem } 
	 */
	public Mensagem agendar(Request req, Response resp) {
		try {
			final Agendamento agendamento = gson.fromJson(req.body(), Agendamento.class);
			final Paciente paciente = pacienteDAO.findByMatricula(agendamento.getPaciente());
			Objects.requireNonNull(paciente, "Matricula inválida");
			final Dentista dentista = paciente.getDentista();
			agendamento.setPaciente(paciente);
			agendamento.setDentista(dentista);
			agendamento.getHorario().setDayOfWeek(agendamento.getHorario().getData().getDayOfWeek());
			
			if (!dentista.atende(agendamento.getHorario())) {
				throw new Exception("Dentista não atende nesse horário");
			}
			verificarVagasPara(agendamento);
			verificarInadiplencia(paciente);
			
			if (agendamentoDAO.save(agendamento)) {
				return new Mensagem("agendamento relizado com sucesso!", true);
			}else {
				throw new Exception("Houve um erro ao tentar realizar o agendamento!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Mensagem(e.getMessage(), false);
		}
	}

	/**
	 * @param paciente
	 * @throws Exception se o paciente tentar agendar com consultas em aberto, ou se não compareceu
	 * ao último agendamento. No segundo caso, o prazo de venciemnto é de 1 mês, ou seja, mesmo que não 
	 * tenha comparecido no último agendamento, após um mês ele poderá realizar outro agendamento 
	 */
	private void verificarInadiplencia(final Paciente paciente) throws Exception{
		LocalDate tolerancia = LocalDate.now().minusMonths(1);
		for (Agendamento a : paciente.getAgendamentos()) {
			if (a.isAtivo())
				throw new Exception("Não é possível agendar com consultas ainda em aberto.");
			else if (!a.compareceu() && a.getHorario().getData().compareTo(tolerancia) > 0) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd 'de' MMMM ", new Locale("pt", "BR"));
				String dateString = a.getHorario().getData().format(formatter);
				throw new Exception("Devido a ausência no último agndamento, "
						+ "Você só poderá realizar um novo agendamento a partir do dia " + dateString);
			}
		}
	}
	
	/**
	 * @param agendamento
	 * @throws Exception se não huver vagas para a data escolhida
	 */
	private void verificarVagasPara(final Agendamento agendamento)
			throws Exception {
		List<Agendamento> agendamentosDoDia = agendamentoDAO.getAgendamentos(agendamento.getHorario().getData());
		agendamentosDoDia = agendamentosDoDia.stream().filter(a -> a.isAtivo()).collect(Collectors.toList());
		if (agendamentosDoDia.size() >= MAX_AGENDAMENTOS_PERMITIDOS || agendamentosDoDia.contains(agendamento))
			throw new Exception("Sem vagas para essa data.");

	}

	//TODO Refatorar para apagar o uso da classe Intervalo
	public List<Agendamento> getAll(Request req, Response resp) {
		try {
			Intervalo intervalo = new Intervalo(req);
			List<Agendamento> agendamentos = agendamentoDAO.getAllBetween(intervalo);
			return agendamentos.stream().filter(a -> a.isAtivo()).collect(Collectors.toList());
		} catch (Exception e) {
			final Logger logger = LoggerFactory.getLogger(AgendamentoController.class);
			logger.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	public List<Agendamento> getAgendamentosPorPaciente(Request req, Response resp) {
		try {
			return agendamentoDAO.getAgendamentosByPaciente(Integer.parseInt(req.params(":id"))).stream()
					.filter(a -> a.isAtivo()).collect(Collectors.toList());
		} catch (Exception e) {
			final Logger logger = LoggerFactory.getLogger(AgendamentoController.class);
			logger.error(e.getMessage());
			return new ArrayList<>();
		}
	}
  //TODO Refatorar para apagar o uso da classe Intervalo 
	public Object getAgendamentosPorDentista(Request req, Response resp) {
		try {
			final int id = Integer.parseInt(req.params(":id"));
			Intervalo intervalo = new Intervalo(req);
			return agendamentoDAO.getAgendamentosByDentista(id, intervalo);
		} catch (Exception e) {
			final Logger logger = LoggerFactory.getLogger(AgendamentoController.class);
			logger.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	public Agendamento getAgendamento(Request req, Response resp) {
		try {
			return agendamentoDAO.get(Integer.parseInt(req.params(":id")));
		} catch (Exception e) {
			Logger logger = LoggerFactory.getLogger(AgendamentoController.class);
			logger.error(e.getMessage());
			return null;
		}
	}

	public Object[] getHorariosDisponiveis(Request req) {
		final LocalDate date = LocalDate.parse(req.params(":data"), DateTimeFormatter.ofPattern("dd-M-yyy"));
		int dentistaId = Integer.parseInt(req.params(":dentistaID"));
		return getHorariosDisponiveis(date, dentistaId).toArray();
	}

	public Stream<Horario> getHorariosDisponiveis(LocalDate date, Integer dentistaId) {
		final Horario horario = new Horario();
		horario.setData(date);
		horario.setDayOfWeek(horario.getData().getDayOfWeek());
		final ArrayList<Horario> horariosOcupados = (ArrayList<Horario>) agendamentoDAO
				.getHorariosOcupadosPorData(horario.getData());
		final Dentista dentista = dentistaDAO.get(dentistaId);

		return dentista.getHorariosDeAtendimento().stream()
				.filter(h -> horarioLivre(horario.getDayOfWeek(), horariosOcupados, h));
	}

	private boolean horarioLivre(final DayOfWeek dayOfWeek, final ArrayList<Horario> horariosOcupados, Horario h) {
		return !horariosOcupados.contains(h) && h.getDayOfWeek().equals(dayOfWeek);
	}

	public Mensagem update(Request req, Response resp) {
		try {
			Gson gson = GsonUtil.getInstance();
			Agendamento agendamento = gson.fromJson(req.body(), Agendamento.class);
			agendamentoDAO.update(agendamento);
			return new Mensagem("Agendamento atualizado com sucesso!", true);
		} catch (Exception e) {
			return new Mensagem("Erro ao tentar atualizar o orçamento!", false);
		}
	}
}
