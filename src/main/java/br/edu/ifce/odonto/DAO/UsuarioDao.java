package br.edu.ifce.odonto.DAO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.ifce.odonto.models.Usuario;
import br.edu.ifce.odonto.util.JPAUtil;

public class UsuarioDao {
	private EntityManager entityManager = JPAUtil.getEntityManager();
	Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());
	
	public Usuario get(Usuario usuario) {
		String str = "SELECT u FROM Usuario u WHERE u.matricula = :pMatricula and u.password = :pPassword";
		TypedQuery<Usuario> query = entityManager.createQuery(str, Usuario.class);
		query.setParameter("pMatricula", usuario.getMatricula());
		query.setParameter("pPassword", usuario.getPassword());
		try {
			Usuario result = query.getSingleResult();			
			return result;
		}catch (NoResultException nre) {
			nre.printStackTrace();
			return null;
		}
	}

	public void save(Usuario user) throws Exception {
		try {	
			Usuario usuario = get(user);
			if(usuario != null)
				throw new Exception("Já existe um usuário com essa matricula");
			entityManager.persist(user);
		}catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage() +" Já existe um usuário com essa matricula");	
			throw new Exception(e.getMessage());	
		}
	}
}
