package br.edu.ifce.odonto.DAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.edu.ifce.odonto.exceptios.ConstraintViolationException;
import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.util.JPAUtil;

public class DentistaDAO {

	private EntityManager entityManager = JPAUtil.getEntityManager();
	

	public boolean save(Dentista dentista) throws Exception {
		try {
			entityManager.persist(dentista);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ConstraintViolationException("Já existe um dentista cadastrado com o cro " + dentista.getCro());
		}
	}

	/**
	 * retorna um dentista ou nulo caso não for encontrado nenhum dentista com o id
	 * passado
	 * 
	 * @param id - do dentista
	 * 
	 * @return Dentista
	 */
	public Dentista get(Integer id) {
		try {
			Dentista dentista = entityManager.find(Dentista.class, id);
			return dentista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public Collection<Dentista> getAll() {
		try {
			final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			final CriteriaQuery<Dentista> query = criteriaBuilder.createQuery(Dentista.class);
			query.from(Dentista.class);
			final TypedQuery<Dentista> typedQuery = entityManager.createQuery(query);
			final List<Dentista> resultList = typedQuery.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Dentista>();
		}

	}

	public boolean update(Dentista dentista) {
		try {
			entityManager.merge(dentista);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Dentista findByCRO(String cro) {
		try {
			TypedQuery<Dentista> query = entityManager.createQuery("SELECT d FROM Dentista d WHERE d.cro = :cro",
					Dentista.class);
			query.setParameter("cro", cro);
			Dentista dentista = query.getSingleResult();
			return dentista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public boolean delete(Dentista dentista) {
		try {
			entityManager.remove(dentista);	
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
