package br.edu.ifce.odonto.DAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.ifce.odonto.models.Dependente;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.models.Servidor;
import br.edu.ifce.odonto.util.AuthorizationService;
import br.edu.ifce.odonto.util.JPAUtil;

public class PacienteDAO {

	private EntityManager entityManager = JPAUtil.getEntityManager();

	Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

	public void save(Paciente paciente) throws Exception {
		try {
			if (paciente.getTipo() == null)
				throw new IllegalArgumentException("O argumento tipo tem valor inválido");
			entityManager.merge(paciente);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public Paciente get(Paciente paciente) {
		try {
			Paciente find = entityManager.find(Paciente.class, paciente.getId());
			return find;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public Collection<Paciente> getAll(final int pagina, final int max) {
		try {
			if (max > 100)
				return null;
			TypedQuery<Paciente> query = entityManager.createQuery("SELECT p FROM Paciente p order by p.nome",
					Paciente.class);
			query.setFirstResult(max * pagina);
			query.setMaxResults(max);
			List<Paciente> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Paciente>();
		}

	}

	public Paciente findByMatricula(Paciente p) {
		try {
			TypedQuery<Paciente> query = entityManager
					.createQuery("SELECT p FROM Paciente p WHERE p.matricula = :pMatricula", Paciente.class);
			query.setParameter("pMatricula", p.getMatricula());
			Paciente paciente = query.getSingleResult();
			return paciente;
		} catch (NoResultException nre) {
			logger.info("Paciente não encontrado em: " + nre.getStackTrace()[1].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean update(Paciente paciente) {
		try {
			entityManager.merge(paciente);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Paciente findByMatriculaAndPassword(Paciente p) {
		try {
			TypedQuery<Paciente> query = entityManager.createQuery(
					"SELECT p FROM Paciente p WHERE p.matricula = :pMatricula " + "and p.password = :pPassword",
					Paciente.class);
			query.setParameter("pMatricula", p.getMatricula());
			query.setParameter("pPassword", AuthorizationService.getMD5(p.getPassword()));
			Paciente paciente = query.getSingleResult();
			return paciente;
		} catch (NoResultException nre) {
			logger.info("Paciente não encontrado em: " + nre.getStackTrace()[1].toString());
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Paciente findByEmail(String email) {
		try {
			TypedQuery<Paciente> query = entityManager.createQuery("SELECT p FROM Paciente p WHERE p.email = :pEmail",
					Paciente.class);
			query.setParameter("pEmail", email);
			Paciente paciente = query.getSingleResult();
			return paciente;
		} catch (NoResultException nre) {
			logger.info("Paciente não encontrado em: " + nre.getStackTrace()[1].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Dependente> getDependentes(Integer idTitular) {
		Servidor servidor = entityManager.find(Servidor.class, idTitular);
		TypedQuery<Dependente> query = entityManager.createQuery("SELECT d FROM Dependente d WHERE d.titular = :titular",
				Dependente.class);
		query.setParameter("titular", servidor);
		List<Dependente> dependentes = query.getResultList();
		return dependentes;
	}
}
