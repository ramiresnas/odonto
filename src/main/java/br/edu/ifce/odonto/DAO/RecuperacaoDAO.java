package br.edu.ifce.odonto.DAO;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.edu.ifce.odonto.models.Recuperacao;
import br.edu.ifce.odonto.util.JPAUtil;

public class RecuperacaoDAO {

	private EntityManager entityManager = JPAUtil.getEntityManager();
	
	public boolean save(Recuperacao recuperacao) {
		try {
			PacienteDAO pacienteDAO = new PacienteDAO();
			pacienteDAO.findByEmail(recuperacao.getEmail());
			entityManager.persist(recuperacao);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Optional<Recuperacao> findByCodigo(Recuperacao recuperacao) {
		Optional<Recuperacao> rec;
		try {
			String queryString = "SELECT r FROM Recuperacao r where r.email = :pEmail and r.codigo = :pCodigo "
					+ "order by r.id";
			TypedQuery<Recuperacao> query = entityManager.createQuery(queryString, Recuperacao.class);
			query.setParameter("pEmail", recuperacao.getEmail());
			query.setParameter("pCodigo", recuperacao.getCodigo());
			List<Recuperacao> list = query.getResultList();
			if (list.isEmpty())
				throw new Exception();
			rec = Optional.ofNullable(list.get(0));
			boolean codigosSaoIguais = rec.get().getCodigo().equals(recuperacao.getCodigo());
			if (!codigosSaoIguais) 
				throw new Exception();
		} catch (Exception e) {
			e.printStackTrace();
			rec = Optional.empty();
		}
		return rec;
	}

	public boolean remove(Recuperacao recuperacao) {
		try {
			entityManager.remove(recuperacao);
			return true;
		}catch (Exception e) { 
			e.printStackTrace();
			return false;
		}
	}
}
