package br.edu.ifce.odonto.DAO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.edu.ifce.odonto.models.Agendamento;
import br.edu.ifce.odonto.models.Dentista;
import br.edu.ifce.odonto.models.Discente;
import br.edu.ifce.odonto.models.Horario;
import br.edu.ifce.odonto.models.Intervalo;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.util.JPAUtil;

public class AgendamentoDAO {
	private EntityManager entityManager = JPAUtil.getEntityManager();


	public boolean save(Agendamento agendamento) {
		try {
			Agendamento agendamentoResult = entityManager.merge(agendamento);
			return agendamentoResult != null;
		} catch (Exception e) {
			return false;
		}
		
	}

	public Agendamento get(int id) {
		try {
			String queryString = "SELECT a FROM Agendamento a where a.id = :pId";
			TypedQuery<Agendamento> query = entityManager.createQuery(queryString,Agendamento.class);
			query.setParameter("pId", id);
			Agendamento singleResult = query.getSingleResult();
			return singleResult;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	public List<Agendamento> getAgendamentosByPaciente(int pacienteID) {
		try {
			String queryString = "SELECT a FROM Agendamento a where a.paciente = :pPaciente";
			TypedQuery<Agendamento> query = entityManager.createQuery(queryString,Agendamento.class);
			Discente discente = new Discente();
			discente.setId(pacienteID);
			PacienteDAO pacienteDAO = new PacienteDAO();
			query.setParameter("pPaciente", pacienteDAO.get(discente));
			List<Agendamento> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Agendamento>();
		}
	}

	//TODO Refatorar para apagar o uso da classe Intervalo
	public List<Agendamento> getAgendamentosByDentista(int dentistaID,Intervalo intervalo) {
		try {
			String queryString = "SELECT a FROM Agendamento a where a.dentista = :pDentista and "
					+ " a.horario.data between :dataInicio and :dataFim";
			TypedQuery<Agendamento> query = entityManager.createQuery(queryString,Agendamento.class);
			DentistaDAO dentistaDAO = new DentistaDAO();
			query.setParameter("pDentista", dentistaDAO.get(dentistaID));
			query.setParameter("dataInicio", intervalo.getInicio());
			query.setParameter("dataFim", intervalo.getFim());
			List<Agendamento> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Agendamento>();
		}
		
	}

	//TODO Refatorar para apagar o uso da classe Intervalo
	public List<Agendamento> getAllBetween(Intervalo intervalo) {
		try {
			String queryString = "SELECT a FROM Agendamento a where a.horario.data BETWEEN :pInicio AND :pFim";
			TypedQuery<Agendamento> query = entityManager.createQuery(queryString,Agendamento.class);
			query.setParameter("pInicio", intervalo.getInicio());
			query.setParameter("pFim", intervalo.getFim());
			List<Agendamento> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Agendamento>();
		}
		
	}

	public List<Horario> getHorariosOcupadosPorData(LocalDate data) {
		try {
			String queryString = "SELECT a.horario FROM Agendamento a where a.horario.data = :pData and a.horario.ativo = :pAtivo";
			TypedQuery<Horario> query = entityManager.createQuery(queryString,Horario.class);
			query.setParameter("pData", data);
			query.setParameter("pAtivo", true);
			List<Horario> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Horario>();
		}
		
	}

	public List<Agendamento> getAgendamentos(LocalDate data) {
		try {
			String queryString = "SELECT a FROM Agendamento a where a.horario.data = :pData";
			TypedQuery<Agendamento> query = entityManager.createQuery(queryString,Agendamento.class);
			query.setParameter("pData", data);
			List<Agendamento> resultList = query.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Agendamento>();
		}
	}

	public boolean update(Agendamento agendamento) {
		try {
			Dentista dentista = entityManager.merge(agendamento.getDentista());
			Paciente paciente = entityManager.merge(agendamento.getPaciente());
			agendamento.setPaciente(paciente);
			agendamento.setDentista(dentista);
			entityManager.merge(agendamento);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

}
