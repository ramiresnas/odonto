package App;

import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.notFound;
import static spark.Spark.options;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;

import java.util.List;
import java.util.function.Function;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import br.edu.ifce.odonto.DAO.PacienteDAO;
import br.edu.ifce.odonto.controllers.AcessoController;
import br.edu.ifce.odonto.controllers.AgendamentoController;
import br.edu.ifce.odonto.controllers.DentistaController;
import br.edu.ifce.odonto.controllers.PacienteController;
import br.edu.ifce.odonto.controllers.UsuarioController;
import br.edu.ifce.odonto.models.Dependente;
import br.edu.ifce.odonto.models.Mensagem;
import br.edu.ifce.odonto.models.Paciente;
import br.edu.ifce.odonto.util.AuthorizationService;
import br.edu.ifce.odonto.util.GsonUtil;
import br.edu.ifce.odonto.util.JPAUtil;
import spark.Spark;

public class MainApp {

	private final static Gson gson = GsonUtil.getInstance();
	private final static Logger logger = LoggerFactory.getLogger(MainApp.class);

	public static void main(String args[]) {
		
		port(getHerokuAssignedPort());
		// init listen end points
		pacientes();
		dentistas();
		agendamentos();
		horarios();
		usuarios();
		acesso();
		servidor();

		// this method call the method beforeFilter
		enableCORS("*", "GET,PUT,DELETE,POST,OPTIONS", "Access-Control-Allow-Origin, "
				+ "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

		afterFilters();
		
		Function<String, String> message = uri -> "A url " + uri
				+ " nao esta mapeada no servidor, verifique a url e tente novamente";

		notFound((req, resp) -> gson.toJson(new Mensagem(message.apply(req.uri()), false)));

		logger.info("servidor inicializado");
	}

	private static void afterFilters() {
		after((request, response) -> response.header("Content-Encondig", "gzip"));
		
		//close EntityManager
		Spark.afterAfter("*", (req,resp)->{ 
			EntityManager entityManager = JPAUtil.getEntityManager();
			try {
				if( entityManager.isOpen() && entityManager.getTransaction().isActive())
					entityManager.getTransaction().commit();
			}catch (Exception e) {
				entityManager.getTransaction().rollback();
				Mensagem mensagem = new Mensagem(e.getMessage(), false);
				resp.body(gson.toJson(mensagem));
			}finally {
				if(entityManager.isOpen())
					entityManager.close();
			}
		});
	}
	
	private static void enableCORS(final String origin, final String methods, final String headers) {

		options("/*", (request, response) -> {
			final String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			final String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}

			return new Mensagem("Utilize um endpoint correspondente para acessar a API!", true);
		}, gson::toJson);

		beforeFilter(origin, methods, headers);
	}

	private static void beforeFilter(final String origin, final String methods, final String headers) {
		before((request, response) -> {
			String method = request.requestMethod();
			if(method.equals("POST") || method.equals("PUT") ||  method.equals("DELETE"))
				JPAUtil.getEntityManager().getTransaction().begin();
			
			final boolean urlCadastroOuLogin = request.uri().endsWith("/usuario/add")
					|| request.uri().endsWith("/usuario/logar") || request.uri().endsWith("/paciente/login");
			final boolean isOption = request.requestMethod().equalsIgnoreCase("OPTIONS");

			if (!urlCadastroOuLogin && !isOption) {
				AuthorizationService.authorize(request, response);
			}

			response.header("Access-Control-Allow-Origin", origin);
			response.header("Access-Control-Request-Method", methods);
			response.header("Access-Control-Allow-Headers", headers);
			response.header("Access-Control-Allow-Credentials", "true");
			response.type("application/json");

		});
	}

	private static void pacientes() {
		path("/api/", () -> {
			path("/paciente/", () -> {
				get("pagina/:pagina/quantidade/:quantidade", "application/json",
						(req, resp) -> new PacienteController().getAll(req), gson::toJson);
				get("/:id", (req, resp) -> new PacienteController().get(req, resp), gson::toJson); 
				get("matricula/:matricula", (req, resp) -> {
					final Paciente paciente = new PacienteController().findByMatricula(req, resp);
					if (paciente != null) {
						return paciente;
					} else
						return gson.fromJson(resp.body(), Mensagem.class);
				}, gson::toJson);
				get("/agendamentos/matricula/:matricula",
						(req, resp) -> new PacienteController().getAgendamentos(req, resp), gson::toJson);
				post("/add", (req, resp) -> new PacienteController().addUser(req, resp), gson::toJson);
				post("servidor/dependente/add", (req, resp) -> new PacienteController().addDependente(req, resp),
						gson::toJson);
				post("login", (req, resp) -> new PacienteController().login(req, resp), gson::toJson);
			});
		});
	}
	
	private static void servidor() {
		path("/api/servidor", ()->{
			get("/:id/dependentes", (req,resp)->{
				try {
					int id = Integer.parseInt(req.params(":id"));
					List<Dependente> dependentes = new PacienteDAO().getDependentes(id);
					return new Mensagem("sucesso", true,dependentes);
				}catch (Exception e) {
					return new Mensagem("id inválido, o id deve ser um número inteiro", false);
				}
				
			},gson::toJson);
		});
	}

	private static void dentistas() {
		path("/api", () -> {
			path("/dentista", () -> {
				delete("/remove", "application/json", (req, resp) -> new DentistaController().delete(req, resp),
						gson::toJson);
				get("/", "application/json", (req, resp) -> new DentistaController().getAll(), gson::toJson);
				get("/id/:id", "application/json", (req, resp) -> new DentistaController().get(req, resp),
						gson::toJson);
				get("/cro/:cro", "application/json", (req, resp) -> new DentistaController().findByCRO(req),
						gson::toJson);
				post("/add", (req, resp) -> new DentistaController().addDentista(req, resp), gson::toJson);
				put("/update", (req, resp) -> new DentistaController().update(req), gson::toJson);
			});
		});
	}

	private static void agendamentos() {
		path("/api/", () -> {
			path("/agendamento", () -> {
				get("/:id", (req, resp) -> new AgendamentoController().getAgendamento(req, resp), gson::toJson);
				get("/paciente/:id", (req, resp) -> new AgendamentoController().getAgendamentosPorPaciente(req, resp),
						gson::toJson);
				get("/dentista/:id/:dataInicio/:dataFim",
						(req, resp) -> new AgendamentoController().getAgendamentosPorDentista(req, resp), gson::toJson);
				get("/:dataInicio/:dataFim", (req, resp) -> new AgendamentoController().getAll(req, resp),
						gson::toJson);
				post("/add", "application/json", (req, resp) -> new AgendamentoController().agendar(req, resp),
						gson::toJson);
				put("/update", (req, resp) -> new AgendamentoController().update(req, resp), gson::toJson);
			});
		});
	}

	private static void horarios() {
		path("/api/horarios", () -> {
			get("/:data/:dentistaId", (req, resp) -> new AgendamentoController().getHorariosDisponiveis(req),
					gson::toJson);
		});
	}

	private static void acesso() {
		path("/api/acesso/", () -> {
			AcessoController controller = new AcessoController();
			post("send_code", (req, resp) -> controller.sendCode(req, resp), gson::toJson);
			post("recuperar_senha", (req, resp) -> controller.recuperarSenha(req, resp), gson::toJson);
		});
	}

	private static void usuarios() {
		path("/api/usuario", () -> {
			post("/add", (req, resp) -> new UsuarioController().add(req, resp), gson::toJson);
			post("/logar", (req, resp) -> new UsuarioController().logar(req, resp), gson::toJson);
		});
	}	

	static int getHerokuAssignedPort() {
		final ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get("PORT") != null) {
			final int PORT = Integer.parseInt(processBuilder.environment().get("PORT"));
			return PORT;
		}
		return 4567;
	}

}
